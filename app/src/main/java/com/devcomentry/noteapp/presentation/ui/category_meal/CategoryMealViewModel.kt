package com.devcomentry.noteapp.presentation.ui.category_meal

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.server.meal.GetMealsByCategoryUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.model.MealByCategoryItem
import com.devcomentry.noteapp.presentation.model.MealByCategoryItemMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryMealViewModel @Inject constructor(
    private val useCase: GetMealsByCategoryUseCase,
    private val mapper: MealByCategoryItemMapper
) : BaseViewModel() {

    private var _mealList = MutableLiveData<List<MealByCategoryItem>>()
    val mealList = _mealList as LiveData<List<MealByCategoryItem>>
    fun getMealByCategory(name: String) {
        parentJob = viewModelScope.launch(handler) {
            val result = useCase.invoke(name).map {
                mapper.mapToPresentation(it)
            }
            _mealList.value = result
        }
        registerJobFinish()
    }
}