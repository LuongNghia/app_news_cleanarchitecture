package com.devcomentry.noteapp.presentation.ui.home.fragment.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.ItemExploresBinding
import com.devcomentry.noteapp.presentation.model.MealItem

class FavouritesHomeAdapter(
    private val listener: OnItemCLick
) : ListAdapter<MealItem, FavouritesHomeAdapter.MyHolder>(FavoriteDiffUtil) {

    inner class MyHolder(val binding: ItemExploresBinding) : RecyclerView.ViewHolder(binding.root)

    private object FavoriteDiffUtil : DiffUtil.ItemCallback<MealItem>() {
        override fun areItemsTheSame(oldItem: MealItem, newItem: MealItem): Boolean {
            return oldItem.idMeal == newItem.idMeal
        }

        override fun areContentsTheSame(oldItem: MealItem, newItem: MealItem): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            ItemExploresBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val meal = getItem(position)
        Glide.with(holder.itemView)
            .load(meal.strMealThumb)
            .into(holder.binding.imgItems)

        holder.itemView.setOnClickListener {
            meal.strMeal?.let { strMeal ->
                meal.strMealThumb?.let { strThumb ->
                    listener.itemFavouriteCLick(
                        meal.idMeal,
                        strMeal,
                        strThumb
                    )
                }
            }
        }
    }

    override fun onViewRecycled(holder: MyHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView).clear(holder.binding.imgItems)
    }

    interface OnItemCLick {
        fun itemFavouriteCLick(strId: String, strName: String, strThumb: String)
    }
}