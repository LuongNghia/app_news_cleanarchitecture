package com.devcomentry.noteapp.presentation.ui.chat_bot

import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.ActivityChatBotBinding
import com.devcomentry.noteapp.presentation.base.activity.BaseActivity
import com.devcomentry.noteapp.presentation.common.CommonUtils.hideSoftKeyboard
import com.devcomentry.noteapp.presentation.model.MessageItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChatBotActivity : BaseActivity<ActivityChatBotBinding>() {

    private var chatList: ArrayList<MessageItem> = ArrayList()
    private val viewModel by viewModels<ChatBotViewModel>()
    private lateinit var adapter: ChatBotAdapter
    override fun getContentLayout(): Int {
        return R.layout.activity_chat_bot
    }

    override fun initView() {
        chatList = ArrayList()
        binding.apply {
            toolbar.tvTitle.text = getString(R.string.chat_bot_activity)
            toolbar.imgBack.setOnClickListener {
                finish()
            }
            chatList.add(MessageItem("assistant", "Mời bạn nhập câu hỏi muốn hỏi"))
            adapter = ChatBotAdapter(chatList)
            rcvMessages.layoutManager = LinearLayoutManager(this@ChatBotActivity)
            rcvMessages.setHasFixedSize(true)
            rcvMessages.adapter = adapter
        }

    }

    override fun initListener() {
        binding.apply {
            btnSend.setOnClickListener {
                val message = edtMessage.text.toString()
                if (message.isNullOrEmpty()) {
                    showToast(this@ChatBotActivity, "Null", 0)
                } else {
                    chatList.add(
                        MessageItem(
                            role = "user",
                            content = message
                        )
                    )
                    adapter.notifyDataSetChanged()
                    viewModel.sendChatRequest(
                        MessageItem(
                            role = "user",
                            content = message
                        )
                    )
                    edtMessage.text.clear()
                    hideSoftKeyboard()
                }
            }
        }
    }

    override fun observerLiveData() {
        viewModel.chatResponse.observe(this@ChatBotActivity) {
            chatList.add(it)
            adapter.notifyDataSetChanged()
        }
    }
}