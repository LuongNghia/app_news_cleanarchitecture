package com.devcomentry.noteapp.presentation.ui.chat_bot

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.server.chat_bot.SendChatUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.model.MessageItem
import com.devcomentry.noteapp.presentation.model.MessageItemMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChatBotViewModel @Inject constructor(
    private val useCase: SendChatUseCase,
    private val mapper: MessageItemMapper
) : BaseViewModel() {

    private var _chatResponse = MutableLiveData<MessageItem>()
    val chatResponse = _chatResponse as LiveData<MessageItem>
    fun sendChatRequest(message: MessageItem) {
        parentJob = viewModelScope.launch(handler) {
            val domainMessage = useCase.invoke(mapper.mapToDomain(message))
            val itemMessage = mapper.mapToPresentation(domainMessage)
            _chatResponse.value = itemMessage
        }
        registerJobFinish()
    }

}