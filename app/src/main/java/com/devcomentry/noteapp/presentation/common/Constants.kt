package com.devcomentry.noteapp.presentation.common

const val FORMAT_DATE: String = "HH:mm dd/MM/yyyy"
const val KEY_OBJECT = "KEY_OBJECT"
const val TAG_SPLASH = "SPLASH_ACTIVITY"
const val EMPTY_STRING = ""
const val INIT_VIEW_DELAY = 10L
const val MEAL_ID = "MEAL_ID"
const val MEAL_NAME = "MEAL_NAME"
const val MEAL_THUMB = "MEAL_THUMB"
const val CATEGORY_NAME = "CATEGORY_NAME"
const val NOTE_ID = "NOTE_ID"

