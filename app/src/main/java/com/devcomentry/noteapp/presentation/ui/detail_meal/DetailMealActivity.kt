package com.devcomentry.noteapp.presentation.ui.detail_meal

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.ActivityDetailMealBinding
import com.devcomentry.noteapp.presentation.base.activity.BaseActivity
import com.devcomentry.noteapp.presentation.common.EMPTY_STRING
import com.devcomentry.noteapp.presentation.common.MEAL_ID
import com.devcomentry.noteapp.presentation.common.MEAL_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_THUMB
import com.devcomentry.noteapp.presentation.model.MealItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailMealActivity : BaseActivity<ActivityDetailMealBinding>() {

    private val viewModel by viewModels<DetailMealViewModel>()
    private lateinit var mealId: String
    private lateinit var mealName: String
    private lateinit var mealThumb: String
    private lateinit var mealYoutube: String
    private var mealToSave: MealItem? = null

    override fun getContentLayout(): Int {
        return R.layout.activity_detail_meal
    }

    override fun initView() {
        //onFavouriteClick()

        val intent = intent
        mealId = intent.getStringExtra(MEAL_ID) ?: EMPTY_STRING
        mealName = intent.getStringExtra(MEAL_NAME) ?: EMPTY_STRING
        mealThumb = intent.getStringExtra(MEAL_THUMB) ?: EMPTY_STRING

        binding.apply {
            lltoolbar.tvTitle.text = mealName
            Glide.with(applicationContext).load(mealThumb).into(imgMealDetails)
            collapsingToolbar.title = mealName
            collapsingToolbar.setCollapsedTitleTextColor(resources.getColor(R.color.white))
            collapsingToolbar.setExpandedTitleColor(resources.getColor(R.color.white))
            progressBar.visibility = View.VISIBLE
            btnAddFavourite.visibility = View.INVISIBLE
            tvInstructions.visibility = View.INVISIBLE
            tvCategories.visibility = View.INVISIBLE
            tvArea.visibility = View.INVISIBLE
            imgYoutube.visibility = View.INVISIBLE
        }
        viewModel.getMealDetail(mealId)
    }

    private fun onResponseCase() {
        binding.progressBar.visibility = View.INVISIBLE
        binding.btnAddFavourite.visibility = View.VISIBLE
        binding.tvInstructions.visibility = View.VISIBLE
        binding.tvCategories.visibility = View.VISIBLE
        binding.tvArea.visibility = View.VISIBLE
        binding.imgYoutube.visibility = View.VISIBLE
    }

    override fun initListener() {
        binding.apply {
            imgYoutube.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(mealYoutube))
                startActivity(intent)
            }
            btnAddFavourite.setOnClickListener {
                mealToSave?.let {
                    viewModel.insertMealFavourite(it)
                }
            }
            lltoolbar.imgBack.setOnClickListener {
                finish()
            }
        }
    }

    override fun observerLiveData() {
        viewModel.getDetailMealResponse.observe(this@DetailMealActivity) {
            onResponseCase()
            mealToSave = it
            binding.tvCategories.text = "Category: ${it.strCategory}"
            binding.tvArea.text = "Area: ${it.strArea}"
            binding.tvInstructionsSt.text = it.strInstructions
            mealYoutube = it.strYoutube.toString()
        }
        viewModel.messageResponse.observe(this@DetailMealActivity) {
            showToastSingleText("Meal saved")
        }
    }
}