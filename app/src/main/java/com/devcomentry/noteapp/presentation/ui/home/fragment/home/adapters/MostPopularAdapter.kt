package com.devcomentry.noteapp.presentation.ui.home.fragment.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.ItemMostPopularBinding
import com.devcomentry.noteapp.presentation.model.MealsByCategoryItem

class MostPopularAdapter(
    private val listener: OnItemCLick,
) : ListAdapter<MealsByCategoryItem, MostPopularAdapter.MyHolder>(PopularDiffUtil) {

    var onLongItemClick: ((MealsByCategoryItem) -> Unit)? = null

    class MyHolder(var binding: ItemMostPopularBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            ItemMostPopularBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val data = getItem(position)
        Glide.with(holder.itemView)
            .load(data.strMealThumb)
            .into(holder.binding.imgPopularItems)
        holder.binding.apply {
            tvArea.text = ""
            tvName.text = data.strMeal
        }

        holder.itemView.setOnClickListener {
            listener.itemCLick(
                data.idMeal,
                data.strMeal,
                data.strMealThumb,
            )
        }

        holder.itemView.setOnLongClickListener {
            onLongItemClick?.invoke(data)
            true
        }

    }

    override fun onViewRecycled(holder: MyHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView).clear(holder.binding.imgPopularItems)
    }


    interface OnItemCLick {
        fun itemCLick(strId: String, strName: String, strThumb: String)
    }

    private object PopularDiffUtil : DiffUtil.ItemCallback<MealsByCategoryItem>() {

        override fun areItemsTheSame(
            oldItem: MealsByCategoryItem,
            newItem: MealsByCategoryItem
        ): Boolean {
            return oldItem.idMeal == newItem.idMeal

        }

        override fun areContentsTheSame(
            oldItem: MealsByCategoryItem,
            newItem: MealsByCategoryItem
        ): Boolean {
            return oldItem == newItem
        }
    }
}