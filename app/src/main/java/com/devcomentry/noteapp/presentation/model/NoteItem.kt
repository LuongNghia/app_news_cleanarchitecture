package com.devcomentry.noteapp.presentation.model

import com.devcomentry.noteapp.domain.model.Note
import com.devcomentry.noteapp.presentation.base.mapper.ItemMapper
import javax.inject.Inject

data class NoteItem(
    var id: String,
    val noteTitle: String,
    val noteSubtitle: String,
    val note: String,
    val dateTime: String,
) : ModelItem()

class NoteItemMapper @Inject constructor() : ItemMapper<Note, NoteItem> {
    override fun mapToPresentation(model: Note): NoteItem {
        return NoteItem(
            id = model.id,
            dateTime = model.dateTime,
            noteSubtitle = model.noteSubtitle,
            note = model.note,
            noteTitle = model.noteTitle
        )
    }

    override fun mapToDomain(model: NoteItem): Note {
        return Note(
            id = model.id,
            dateTime = model.dateTime,
            noteSubtitle = model.noteSubtitle,
            note = model.note,
            noteTitle = model.noteTitle
        )
    }

}