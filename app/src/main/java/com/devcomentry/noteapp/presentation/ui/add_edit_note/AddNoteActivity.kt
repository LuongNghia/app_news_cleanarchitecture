package com.devcomentry.noteapp.presentation.ui.add_edit_note

import androidx.activity.viewModels
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.ActivityNoteBinding
import com.devcomentry.noteapp.presentation.base.activity.BaseActivity
import com.devcomentry.noteapp.presentation.model.NoteItem
import com.devcomentry.noteapp.presentation.ui.home.fragment.note.NoteViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AddNoteActivity : BaseActivity<ActivityNoteBinding>() {
    private val viewModel by viewModels<NoteViewModel>()
    override fun getContentLayout(): Int {
        return R.layout.activity_note
    }

    override fun initView() {
        binding.textDateTime.text =
            SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm a", Locale.getDefault())
                .format(Date())
    }

    override fun initListener() {
        binding.apply {
            btnAddNote.setOnClickListener {
                val id = System.currentTimeMillis().toString()
                val title = inputNoteTitle.text.toString()
                val subTitle = inputNoteSubtitle.text.toString()
                val note = inputNote.text.toString()
                val time = textDateTime.text.toString()
                if (title.isNotEmpty() || subTitle.isNotEmpty() || note.isNotEmpty()) {
                    viewModel.insertNote(
                        NoteItem(
                            id, title, subTitle, note, time
                        )
                    )

                } else {
                    Snackbar.make(
                        it,
                        "Please enter Title or Subtitle or Note",
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }

            imageBack.setOnClickListener {
                finish()
            }
        }
    }

    override fun observerLiveData() {
        viewModel.messageResponse.observe(this@AddNoteActivity) {
            finish()
        }
    }
}