package com.devcomentry.noteapp.presentation.ui.home.fragment.favourite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.local.favourite.DeleteMealsFavouriteUseCase
import com.devcomentry.noteapp.domain.use_case.local.favourite.GetMealsFavouriteUseCase
import com.devcomentry.noteapp.domain.use_case.local.favourite.InsertMealsFavouriteUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.common.MessageType
import com.devcomentry.noteapp.presentation.model.MealItem
import com.devcomentry.noteapp.presentation.model.MealItemMapper
import com.devcomentry.noteapp.presentation.model.Message
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavouriteViewModel @Inject constructor(
    private val getMealsFavouriteUseCase: GetMealsFavouriteUseCase,
    private val insertUsecase: InsertMealsFavouriteUseCase,
    private val deleteUseCase: DeleteMealsFavouriteUseCase,
    private val mapper: MealItemMapper
) : BaseViewModel() {

    private var _mealsFavourite = MutableLiveData<List<MealItem>>()
    val mealsFavouriteResponse: LiveData<List<MealItem>>
        get() = _mealsFavourite

    private var _messageInsert = MutableLiveData<Message>()
    val messageInsertResponse: LiveData<Message>
        get() = _messageInsert

    fun getAllMealFavourite() {
        parentJob = viewModelScope.launch(handler) {
            val result = getMealsFavouriteUseCase.invoke().map {
                mapper.mapToPresentation(it)
            }
            _mealsFavourite.value = result
        }
        registerJobFinish()
    }

    fun deleteMealFavourite(meal: MealItem) {
        parentJob = viewModelScope.launch(handler) {
            val mealMapper = mapper.mapToDomain(meal)
            deleteUseCase.invoke(mealMapper)
        }
        registerJobFinish()
    }

    fun insertMealFavourite(meal: MealItem) {
        parentJob = viewModelScope.launch(handler) {
            val mealMapper = mapper.mapToDomain(meal)
            insertUsecase.invoke(mealMapper)
            _messageInsert.value = Message(MessageType.Success.name)
        }
        registerJobFinish()
    }

}