package com.devcomentry.noteapp.presentation.model

import com.devcomentry.noteapp.domain.model.Message
import com.devcomentry.noteapp.presentation.base.mapper.ItemMapper
import javax.inject.Inject

data class MessageItem(
    val role: String,
    val content: String
) : ModelItem()

class MessageItemMapper @Inject constructor() : ItemMapper<Message, MessageItem> {
    override fun mapToPresentation(model: Message): MessageItem {
        return MessageItem(
            role = model.role,
            content = model.content
        )
    }

    override fun mapToDomain(model: MessageItem): Message {
        return Message(
            role = model.role,
            content = model.content
        )
    }

}