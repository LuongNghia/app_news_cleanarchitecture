package com.devcomentry.noteapp.presentation.model

import com.devcomentry.noteapp.domain.model.Category
import com.devcomentry.noteapp.presentation.base.mapper.ItemMapper
import javax.inject.Inject

data class CategoryItem(
    val idCategory: String,
    val strCategory: String,
    val strCategoryDescription: String,
    val strCategoryThumb: String
) : ModelItem()

class CategoryItemMapper @Inject constructor() : ItemMapper<Category, CategoryItem> {
    override fun mapToPresentation(entity: Category): CategoryItem {
        return CategoryItem(
            idCategory = entity.idCategory,
            strCategory = entity.strCategory,
            strCategoryThumb = entity.strCategoryThumb,
            strCategoryDescription = entity.strCategoryDescription
        )
    }

    override fun mapToDomain(model: CategoryItem): Category {
        TODO("Not yet implemented")
    }

}