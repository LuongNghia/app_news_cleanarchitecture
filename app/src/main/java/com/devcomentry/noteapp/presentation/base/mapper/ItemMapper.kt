package com.devcomentry.noteapp.presentation.base.mapper

import com.devcomentry.noteapp.domain.model.Model
import com.devcomentry.noteapp.presentation.model.ModelItem


interface ItemMapper<M : Model, MI : ModelItem> {
    fun mapToPresentation(model: M): MI

    fun mapToDomain(model: MI): M
}