package com.devcomentry.noteapp.presentation.ui.home.fragment.favourite.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.ItemMealsBinding
import com.devcomentry.noteapp.presentation.model.MealItem

class FavouritesMealsAdapter(
    private val listener: OnItemCLick
) : ListAdapter<MealItem, FavouritesMealsAdapter.MyHolder>(FavoriteDiffUtil) {

    inner class MyHolder(val binding: ItemMealsBinding) : RecyclerView.ViewHolder(binding.root)

    private object FavoriteDiffUtil : DiffUtil.ItemCallback<MealItem>() {
        override fun areItemsTheSame(oldItem: MealItem, newItem: MealItem): Boolean {
            return oldItem.idMeal == newItem.idMeal
        }

        override fun areContentsTheSame(oldItem: MealItem, newItem: MealItem): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            ItemMealsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val meal = getItem(position)
        Glide.with(holder.itemView)
            .load(meal.strMealThumb)
            .into(holder.binding.imgMeal)
        holder.binding.apply {
            tvName.text = meal.strMeal
            tvArea.text = meal.strArea
            tvCategory.text = meal.strCategory
        }

        holder.itemView.setOnClickListener {
            meal.strMeal?.let { strMeal ->
                meal.strMealThumb?.let { strThumb ->
                    listener.itemCLick(
                        meal.idMeal,
                        strMeal,
                        strThumb
                    )
                }
            }
        }
    }

    override fun onViewRecycled(holder: MyHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView).clear(holder.binding.imgMeal)
    }

    interface OnItemCLick {
        fun itemCLick(strId: String, strName: String, strThumb: String)
    }
}