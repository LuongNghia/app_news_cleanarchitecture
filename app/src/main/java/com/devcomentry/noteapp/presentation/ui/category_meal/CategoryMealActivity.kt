package com.devcomentry.noteapp.presentation.ui.category_meal

import android.content.Intent
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.ActivityCategoryMealBinding
import com.devcomentry.noteapp.presentation.base.activity.BaseActivity
import com.devcomentry.noteapp.presentation.common.CATEGORY_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_ID
import com.devcomentry.noteapp.presentation.common.MEAL_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_THUMB
import com.devcomentry.noteapp.presentation.custom.ItemOffsetDecoration
import com.devcomentry.noteapp.presentation.model.MealByCategoryItem
import com.devcomentry.noteapp.presentation.ui.category_meal.adapter.CategoryMealAdapter
import com.devcomentry.noteapp.presentation.ui.detail_meal.DetailMealActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoryMealActivity : BaseActivity<ActivityCategoryMealBinding>(),
    CategoryMealAdapter.OnItemCLick {

    private val viewModel by viewModels<CategoryMealViewModel>()
    private lateinit var adapter: CategoryMealAdapter
    override fun getContentLayout(): Int {
        return R.layout.activity_category_meal
    }

    override fun initView() {
        val intent = intent
        val categoryName = intent.getStringExtra(CATEGORY_NAME).toString()
        binding.apply {
            toolbar.tvTitle.text = categoryName
            rcvCategoryMeal.layoutManager =
                GridLayoutManager(this@CategoryMealActivity, 2, RecyclerView.VERTICAL, false)
            val itemDecoration =
                ItemOffsetDecoration(this@CategoryMealActivity, R.dimen._10sdp)
            rcvCategoryMeal.addItemDecoration(itemDecoration)
            rcvCategoryMeal.setHasFixedSize(true)

            adapter = CategoryMealAdapter(this@CategoryMealActivity, this@CategoryMealActivity)
            rcvCategoryMeal.adapter = adapter
        }
        viewModel.getMealByCategory(categoryName)
    }

    override fun initListener() {
        binding.toolbar.imgBack.setOnClickListener {
            finish()
        }
    }

    override fun observerLiveData() {
        viewModel.apply {
            mealList.observe(this@CategoryMealActivity) {
                adapter.submitList(it)
            }
        }
    }

    override fun itemClick(meal: MealByCategoryItem) {
        val intent = Intent(this, DetailMealActivity::class.java)
        intent.putExtra(MEAL_ID, meal.idMeal)
        intent.putExtra(MEAL_NAME, meal.strMeal)
        intent.putExtra(MEAL_THUMB, meal.strMealThumb)
        startActivity(intent)
    }
}