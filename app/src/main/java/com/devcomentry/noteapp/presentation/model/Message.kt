package com.devcomentry.noteapp.presentation.model

data class Message (
    val message: String
)