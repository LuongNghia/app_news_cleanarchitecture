package com.devcomentry.noteapp.presentation.ui.home.fragment.note

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.local.note.NoteUseCases
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.model.Message
import com.devcomentry.noteapp.presentation.model.NoteItem
import com.devcomentry.noteapp.presentation.model.NoteItemMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NoteViewModel @Inject constructor(
    private val mapper: NoteItemMapper,
    private val useCases: NoteUseCases
) : BaseViewModel() {


    private var _getNotes = MutableLiveData<List<NoteItem>>()
    val getNotes = _getNotes as LiveData<List<NoteItem>>
    fun getAllNotes() {
        parentJob = viewModelScope.launch(handler) {
            val result = useCases.getNotes().map {
                mapper.mapToPresentation(it)
            }
            _getNotes.value = result
        }
        registerJobFinish()
    }

    private var _message = MutableLiveData<Message>()
    val messageResponse: LiveData<Message>
        get() = _message

    fun insertNote(noteItem: NoteItem) {
        viewModelScope.launch {
            val noteDomain = mapper.mapToDomain(noteItem)
            useCases.addNote(noteDomain)
            _message.value = Message("Success")
        }
    }

    private var _getNoteResponse = MutableLiveData<NoteItem>()
    val getNoteResponse = _getNoteResponse as LiveData<NoteItem>
    fun getNoteById(id: String) {
        viewModelScope.launch {
            val note = useCases.getNote(id)
            if (note != null) {
                val noteItem = mapper.mapToPresentation(note)
                _getNoteResponse.value = noteItem
            } else {
                _message.value = Message("Error")
            }
        }
    }

    private var _deleteResponse = MutableLiveData<Boolean>()
    val deleteResponse = _deleteResponse as LiveData<Boolean>
    fun deleteNote(noteItem: NoteItem) {
        viewModelScope.launch {
            val note = mapper.mapToDomain(noteItem)
            useCases.deleteNote(note)
            _deleteResponse.value = true
        }
    }

    private var _updateResponse = MutableLiveData<Boolean>()
    val updateResponse = _updateResponse as LiveData<Boolean>
    fun updateNote(noteItem: NoteItem) {
        viewModelScope.launch {
            val note = mapper.mapToDomain(noteItem)
            useCases.updateNote(note)
            _updateResponse.value = true
        }
    }
}