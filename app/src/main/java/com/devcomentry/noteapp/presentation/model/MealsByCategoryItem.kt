package com.devcomentry.noteapp.presentation.model

import com.devcomentry.noteapp.domain.model.MealsByCategory
import com.devcomentry.noteapp.presentation.base.mapper.ItemMapper
import javax.inject.Inject

data class MealsByCategoryItem (
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
): ModelItem()

class MealsByCategoryItemMapper @Inject constructor() : ItemMapper<MealsByCategory, MealsByCategoryItem> {
    override fun mapToPresentation(entity: MealsByCategory): MealsByCategoryItem {
        return MealsByCategoryItem(
            idMeal = entity.idMeal,
            strMeal = entity.strMeal,
            strMealThumb = entity.strMealThumb,
        )
    }

    override fun mapToDomain(model: MealsByCategoryItem): MealsByCategory {
        TODO("Not yet implemented")
    }

}