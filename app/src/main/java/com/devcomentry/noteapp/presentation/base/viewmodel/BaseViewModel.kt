package com.devcomentry.noteapp.presentation.base.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job

open class BaseViewModel : ViewModel() {

    var parentJob: Job? = null
        protected set

    protected fun registerJobFinish(){
        parentJob?.invokeOnCompletion {

        }
    }

    val handler = CoroutineExceptionHandler { _, exception ->
        print(exception)
    }

    open fun fetchData() {

    }
}