package com.devcomentry.noteapp.presentation.ui.chat_bot

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.devcomentry.noteapp.databinding.ItemMessageBinding
import com.devcomentry.noteapp.presentation.model.MessageItem

class ChatBotAdapter(private val list: ArrayList<MessageItem>) :
    RecyclerView.Adapter<ChatBotAdapter.ViewHolder>() {

    inner class ViewHolder(private val binding: ItemMessageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val data = list[position]
            if (data.role == "assistant") {
                binding.tvMessage.visibility = View.GONE
                binding.tvBotMessage.text = data.content
            } else {
                binding.tvBotMessage.visibility = View.GONE
                binding.tvMessage.text = data.content
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

}