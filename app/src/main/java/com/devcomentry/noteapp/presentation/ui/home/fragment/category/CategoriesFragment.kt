package com.devcomentry.noteapp.presentation.ui.home.fragment.category


import android.content.Intent
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.FragmentCategoriesBinding
import com.devcomentry.noteapp.presentation.base.fragment.BaseFragment
import com.devcomentry.noteapp.presentation.common.CATEGORY_NAME
import com.devcomentry.noteapp.presentation.custom.ItemOffsetDecoration
import com.devcomentry.noteapp.presentation.ui.category_meal.CategoryMealActivity
import com.devcomentry.noteapp.presentation.ui.chat_bot.ChatBotActivity
import com.devcomentry.noteapp.presentation.ui.home.fragment.category.adapter.CategoryAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class CategoriesFragment : BaseFragment<FragmentCategoriesBinding>(), CategoryAdapter.OnItemCLick {

    companion object {
        fun newInstance(): CategoriesFragment {
            return CategoriesFragment()
        }
    }

    private val viewModel by viewModels<CategoryViewModel>()
    private lateinit var adapter: CategoryAdapter

    override fun getContentLayout(): Int {
        return R.layout.fragment_categories
    }

    override fun initView() {
        viewModel.getCategories()
        binding.apply {
            tlbCategory.txtTitle.text = resources.getString(R.string.categories)
            rcvCategory.layoutManager =
                GridLayoutManager(requireContext(), 2, RecyclerView.VERTICAL, false)
            rcvCategory.setHasFixedSize(true)
            adapter = CategoryAdapter(
                requireContext(), this@CategoriesFragment
            )
            val itemDecoration =
                ItemOffsetDecoration(requireContext(), R.dimen._10sdp)
            rcvCategory.addItemDecoration(itemDecoration)
            rcvCategory.adapter = adapter
        }

    }

    override fun initListener() {
        binding.apply {
            tlbCategory.imgChatBot.setOnClickListener {
                val intent = Intent(activity, ChatBotActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun observerLiveData() {
        viewModel.getListCategories.observe(this@CategoriesFragment) {
            adapter.submitList(it)
        }
    }

    override fun itemCLick(str: String) {
        val intent = Intent(activity, CategoryMealActivity::class.java)
        intent.putExtra(CATEGORY_NAME, str)
        startActivity(intent)
    }

}