package com.devcomentry.noteapp.presentation.model

data class ChatBotRequest(
    val model: String,
    val messages: List<MessageItem>
)