package com.devcomentry.noteapp.presentation.ui.category_meal.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.ItemCategoryBinding
import com.devcomentry.noteapp.presentation.model.MealByCategoryItem

class CategoryMealAdapter(
    private val context: Context, private val listener: OnItemCLick
) :
    ListAdapter<MealByCategoryItem, CategoryMealAdapter.ViewHolder>(CategoryMealDiffCallBack) {

    inner class ViewHolder(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: MealByCategoryItem) {
            Glide.with(context).load(data.strMealThumb).into(binding.imgMealCategory)
            binding.txtCategory.text = data.strMeal
            binding.root.setOnClickListener {
                listener.itemClick(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemCategoryBinding.inflate(LayoutInflater.from(context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    private object CategoryMealDiffCallBack : DiffUtil.ItemCallback<MealByCategoryItem>() {
        override fun areItemsTheSame(
            oldItem: MealByCategoryItem,
            newItem: MealByCategoryItem
        ): Boolean {
            return oldItem.idMeal == newItem.idMeal
        }

        override fun areContentsTheSame(
            oldItem: MealByCategoryItem,
            newItem: MealByCategoryItem
        ): Boolean {
            return oldItem == newItem
        }
    }


    interface OnItemCLick {
        fun itemClick(meal: MealByCategoryItem)
    }
}