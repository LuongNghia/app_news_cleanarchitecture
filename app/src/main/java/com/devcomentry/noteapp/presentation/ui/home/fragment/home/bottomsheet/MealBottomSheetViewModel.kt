package com.devcomentry.noteapp.presentation.ui.home.fragment.home.bottomsheet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.server.meal.GetDetailMealUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.model.MealItem
import com.devcomentry.noteapp.presentation.model.MealItemMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MealBottomSheetViewModel @Inject constructor(
    private val mapper: MealItemMapper,
    private val useCase: GetDetailMealUseCase,
) : BaseViewModel() {

    private var _detailMealResponse = MutableLiveData<MealItem>()
    val detailMealResponse: LiveData<MealItem>
        get() = _detailMealResponse


    fun getMealDetail(id: String) {
        parentJob = viewModelScope.launch(handler) {
            val result = useCase.invoke(id).map {
                mapper.mapToPresentation(it)
            }
            _detailMealResponse.postValue(result[0])
        }
        registerJobFinish()
    }
}