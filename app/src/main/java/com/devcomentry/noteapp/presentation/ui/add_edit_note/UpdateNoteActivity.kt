package com.devcomentry.noteapp.presentation.ui.add_edit_note

import androidx.activity.viewModels
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.ActivityUpdateNoteBinding
import com.devcomentry.noteapp.presentation.base.activity.BaseActivity
import com.devcomentry.noteapp.presentation.common.NOTE_ID
import com.devcomentry.noteapp.presentation.model.NoteItem
import com.devcomentry.noteapp.presentation.ui.home.fragment.note.NoteViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class UpdateNoteActivity : BaseActivity<ActivityUpdateNoteBinding>() {

    private val viewModel by viewModels<NoteViewModel>()
    private var noteItem: NoteItem? = null
    private var noteId: String? = null
    override fun getContentLayout(): Int {
        return R.layout.activity_update_note
    }

    override fun initView() {
        val intent = intent
        noteId = intent.getStringExtra(NOTE_ID)
        viewModel.getNoteById(noteId.toString())
    }

    override fun initListener() {
        binding.apply {
            imageBack.setOnClickListener {
                finish()
            }
            imageDelete.setOnClickListener {
                if (noteItem != null) {
                    viewModel.deleteNote(noteItem!!)
                } else {
                    showToast(this@UpdateNoteActivity, "Error", 0)
                }
            }

            btnUpdateNote.setOnClickListener {
                val title = inputNoteTitle.text.toString()
                val subTitle = inputNoteSubtitle.text.toString()
                val note = inputNote.text.toString()
                val time = SimpleDateFormat("EEEE, dd MMMM yyyy HH:mm a", Locale.getDefault())
                    .format(Date())
                if (title.isNotEmpty() || subTitle.isNotEmpty() || note.isNotEmpty()) {
                    val noteItem = NoteItem(noteId.toString(), title, subTitle, note, time)
                    viewModel.updateNote(noteItem)
                } else {
                    showToast(this@UpdateNoteActivity, "Please enter Title or Subtitle or Note", 0)
                }
            }
        }
    }

    override fun observerLiveData() {
        viewModel.apply {
            getNoteResponse.observe(this@UpdateNoteActivity) {
                binding.inputNoteTitle.setText(it.noteTitle)
                binding.inputNoteSubtitle.setText(it.noteSubtitle)
                binding.inputNote.setText(it.note)
                binding.textDateTime.text = it.dateTime
                noteItem = it
            }
            messageResponse.observe(this@UpdateNoteActivity) {
                showToast(this@UpdateNoteActivity, "Error", 0)
            }
            deleteResponse.observe(this@UpdateNoteActivity) {
                if (it) {
                    finish()
                }
            }
            updateResponse.observe(this@UpdateNoteActivity) {
                if (it) {
                    finish()
                }
            }
        }
    }

}