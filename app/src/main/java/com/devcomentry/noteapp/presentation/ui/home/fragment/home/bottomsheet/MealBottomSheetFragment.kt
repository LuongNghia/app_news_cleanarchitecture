package com.devcomentry.noteapp.presentation.ui.home.fragment.home.bottomsheet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.FragmentMealBottomSheetBinding
import com.devcomentry.noteapp.presentation.common.MEAL_ID
import com.devcomentry.noteapp.presentation.common.MEAL_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_THUMB
import com.devcomentry.noteapp.presentation.ui.detail_meal.DetailMealActivity
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MealBottomSheetFragment: BottomSheetDialogFragment() {

    private var mealId: String? = null
    private lateinit var binding: FragmentMealBottomSheetBinding
    private val viewModel by viewModels<MealBottomSheetViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mealId = it.getString(MEAL_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMealBottomSheetBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        mealId?.let {
            viewModel.getMealDetail(it)
        }

        observerBottomSheetMeal()

        onBottomSheetDialogClick()

    }

    private fun onBottomSheetDialogClick() {
        binding.bottomSheet.setOnClickListener {
            if (mealName != null && mealThumb != null) {
                val intent = Intent(activity, DetailMealActivity::class.java)
                intent.apply {
                    putExtra(MEAL_ID, mealId)
                    putExtra(MEAL_NAME, mealName)
                    putExtra(MEAL_THUMB, mealThumb)
                }
                startActivity(intent)
            }
        }
    }

    private var mealName: String? = null
    private var mealThumb: String? = null
    private fun observerBottomSheetMeal() {
        viewModel.detailMealResponse.observe(viewLifecycleOwner, Observer { meal ->
            Glide.with(this).load(meal.strMealThumb).into(binding.imgBottomSheet)
            binding.apply {
                tvBottomsheetArea.text = meal.strArea
                tvBottomsheetCategory.text = meal.strCategory
                tvNameBottomsheet.text = meal.strMeal
            }
            mealName = meal.strMeal
            mealThumb = meal.strMealThumb
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String) =
            MealBottomSheetFragment().apply {
                arguments = Bundle().apply {
                    putString(MEAL_ID, param1)
                }
            }
    }
}
