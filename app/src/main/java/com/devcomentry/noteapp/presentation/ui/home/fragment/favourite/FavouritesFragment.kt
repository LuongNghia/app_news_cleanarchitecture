package com.devcomentry.noteapp.presentation.ui.home.fragment.favourite

import android.content.Intent
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.FragmentFavouritesBinding
import com.devcomentry.noteapp.presentation.base.fragment.BaseFragment
import com.devcomentry.noteapp.presentation.common.MEAL_ID
import com.devcomentry.noteapp.presentation.common.MEAL_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_THUMB
import com.devcomentry.noteapp.presentation.ui.chat_bot.ChatBotActivity
import com.devcomentry.noteapp.presentation.ui.detail_meal.DetailMealActivity
import com.devcomentry.noteapp.presentation.ui.home.fragment.favourite.adapter.FavouritesMealsAdapter
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavouritesFragment : BaseFragment<FragmentFavouritesBinding>(),
    FavouritesMealsAdapter.OnItemCLick {
    companion object {
        fun newInstance(): FavouritesFragment {
            return FavouritesFragment()
        }
    }

    private val viewModel by viewModels<FavouriteViewModel>()
    private lateinit var favouritesAdapter: FavouritesMealsAdapter

    override fun getContentLayout(): Int {
        return R.layout.fragment_favourites
    }

    override fun initView() {
        prepareRecyclerView()
        // get All Meal Favourite
        viewModel.getAllMealFavourite()
        binding.tlbFavorite.txtTitle.text = resources.getString(R.string.favorites)

    }

    override fun initListener() {
        binding.apply {
            tlbFavorite.imgChatBot.setOnClickListener {
                val intent = Intent(activity, ChatBotActivity::class.java)
                startActivity(intent)
            }
        }

        //delete and undo meal favourite
        val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ) = true

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val deletedMeal = favouritesAdapter.currentList[position]
                viewModel.deleteMealFavourite(deletedMeal)

                Snackbar.make(requireView(), "Meal Delete", Snackbar.LENGTH_LONG).setAction(
                    "Undo"
                ) {
                    viewModel.insertMealFavourite(deletedMeal)
                }.show()
                viewModel.getAllMealFavourite()
            }
        }

        ItemTouchHelper(itemTouchHelper).attachToRecyclerView(binding.rvFavourites)
    }

    override fun onStart() {
        super.onStart()
        viewModel.getAllMealFavourite()
    }

    override fun observerLiveData() {
        viewModel.apply {
            mealsFavouriteResponse.observe(
                this@FavouritesFragment
            ) { meals ->
                favouritesAdapter.submitList(meals)
            }

            messageInsertResponse.observe(this@FavouritesFragment) {
                viewModel.getAllMealFavourite()
            }
        }
    }

    private fun prepareRecyclerView() {
        favouritesAdapter = FavouritesMealsAdapter(this@FavouritesFragment)
        binding.rvFavourites.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = favouritesAdapter
        }
    }

    override fun itemCLick(strId: String, strName: String, strThumb: String) {
        val intent = Intent(activity, DetailMealActivity::class.java)
        intent.putExtra(MEAL_ID, strId)
        intent.putExtra(MEAL_NAME, strName)
        intent.putExtra(MEAL_THUMB, strThumb)
        startActivity(intent)
    }

}