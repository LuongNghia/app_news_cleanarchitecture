package com.devcomentry.noteapp.presentation.ui.home.fragment.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.local.favourite.GetMealsFavouriteUseCase
import com.devcomentry.noteapp.domain.use_case.server.category.GetCategoryUseCase
import com.devcomentry.noteapp.domain.use_case.server.home.GetPopularItemsUseCase
import com.devcomentry.noteapp.domain.use_case.server.home.GetRandomMealUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.common.CategoryType
import com.devcomentry.noteapp.presentation.model.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val mealUseCase: GetRandomMealUseCase,
    private val mealsByCategoryUseCase: GetPopularItemsUseCase,
    private val mapperMeal: MealItemMapper,
    private val mapperMealsByCategory: MealsByCategoryItemMapper,
    private val categoryUseCase: GetCategoryUseCase,
    private val mapperCategory: CategoryItemMapper,
    private val getMealsFavouriteUseCase: GetMealsFavouriteUseCase,
) : BaseViewModel() {

    private var _randomMeal = MutableLiveData<MealItem>()
    val randomMealResponse: LiveData<MealItem>
        get() = _randomMeal

    private var _mealsByCategory = MutableLiveData<List<MealsByCategoryItem>>()
    val mealsByCategoryResponse: LiveData<List<MealsByCategoryItem>>
        get() = _mealsByCategory

    private var _listCategories = MutableLiveData<List<CategoryItem>>()
    val listCategoriesResponse: LiveData<List<CategoryItem>>
        get() = _listCategories

    private var _mealsFavourite = MutableLiveData<List<MealItem>>()
    val mealsFavouriteResponse: LiveData<List<MealItem>>
        get() = _mealsFavourite

    fun getRandomMeal() {
        parentJob = viewModelScope.launch(handler) {
            val meal = mealUseCase.invoke()
            val mealItem = meal.let { mapperMeal.mapToPresentation(it) }
            _randomMeal.value = mealItem
        }
        registerJobFinish()
    }

    fun getPopularItems() {
        val categoryNameRandom = CategoryType.values().random().name
        parentJob = viewModelScope.launch(handler) {
            val mealsByCategory = mealsByCategoryUseCase.invoke(categoryNameRandom)
            val mealsByCategoryItem =
                mealsByCategory.map { mapperMealsByCategory.mapToPresentation(it) }
            _mealsByCategory.value = mealsByCategoryItem
        }
        registerJobFinish()
    }

    fun getCategories() {
        parentJob = viewModelScope.launch(handler) {
            val result = categoryUseCase.invoke().map {
                mapperCategory.mapToPresentation(it)
            }
            _listCategories.value = result
        }
        registerJobFinish()
    }

    fun getAllMealFavourite() {
        parentJob = viewModelScope.launch(handler) {
            val result = getMealsFavouriteUseCase.invoke().map {
                mapperMeal.mapToPresentation(it)
            }
            _mealsFavourite.value = result
        }
        registerJobFinish()
    }

}