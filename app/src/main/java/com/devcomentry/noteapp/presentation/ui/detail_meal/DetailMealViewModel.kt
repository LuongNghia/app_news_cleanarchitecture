package com.devcomentry.noteapp.presentation.ui.detail_meal

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.local.favourite.InsertMealsFavouriteUseCase
import com.devcomentry.noteapp.domain.use_case.server.meal.GetDetailMealUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.common.MessageType
import com.devcomentry.noteapp.presentation.model.MealItem
import com.devcomentry.noteapp.presentation.model.MealItemMapper
import com.devcomentry.noteapp.presentation.model.Message
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailMealViewModel @Inject constructor(
    private val mapper: MealItemMapper,
    private val useCase: GetDetailMealUseCase,
    private val insertUsecase: InsertMealsFavouriteUseCase,
) : BaseViewModel() {

    private var _message = MutableLiveData<Message>()
    val messageResponse: LiveData<Message>
        get() = _message

    private var detailMealResponse = MutableLiveData<MealItem>()
    val getDetailMealResponse = detailMealResponse as LiveData<MealItem>
    fun getMealDetail(id: String) {
        parentJob = viewModelScope.launch(handler) {
            val result = useCase.invoke(id).map {
                mapper.mapToPresentation(it)
            }
            detailMealResponse.value = result[0]
        }
        registerJobFinish()
    }

    fun insertMealFavourite(meal: MealItem) {
        parentJob = viewModelScope.launch(handler) {
            val mealMapper = mapper.mapToDomain(meal)
            insertUsecase.invoke(mealMapper)
            _message.value = Message(MessageType.Success.name)
        }
        registerJobFinish()
    }

}