package com.devcomentry.noteapp.presentation.ui.home.fragment.note

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.devcomentry.noteapp.databinding.ItemNoteBinding
import com.devcomentry.noteapp.presentation.model.NoteItem

class NoteAdapter(
    private val listener: OnItemClick
) : ListAdapter<NoteItem, NoteAdapter.ViewHolder>(NoteDiffUtils) {
    inner class ViewHolder(private val binding: ItemNoteBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NoteItem) {
            binding.apply {
                textTitle.text = item.noteTitle
                textSubtitle.text = item.noteSubtitle
                textDateTime.text = item.dateTime
                textContent.text = item.note
                root.setOnClickListener {
                    listener.onNoteClick(item.id)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    private object NoteDiffUtils : DiffUtil.ItemCallback<NoteItem>() {
        override fun areItemsTheSame(oldItem: NoteItem, newItem: NoteItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: NoteItem, newItem: NoteItem): Boolean {
            return oldItem == newItem
        }

    }

    interface OnItemClick {
        fun onNoteClick(noteId: String)
    }
}