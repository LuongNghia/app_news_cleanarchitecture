package com.devcomentry.noteapp.presentation.common

enum class CategoryType {
    Beef,
    Chicken,
    Dessert,
    Lamb,
    Miscellaneous,
    Pasta,
    Pork,
    Seafood,
    Side,
    Starter,
    Vegan,
    Vegetarian,
    Breakfast,
    Goat,
    // get random name: val a = CategoryType.values().random().name
}