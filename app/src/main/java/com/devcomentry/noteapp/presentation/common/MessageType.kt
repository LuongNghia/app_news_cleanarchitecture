package com.devcomentry.noteapp.presentation.common

enum class MessageType {
    Failed,
    Success,
    Processing,
    Error
}