package com.devcomentry.noteapp.presentation.ui.home.fragment.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.ItemPopularBinding
import com.devcomentry.noteapp.presentation.model.CategoryItem

class CategoriesShowHomeAdapter(
    private val listener: OnItemCLick
) : ListAdapter<CategoryItem, CategoriesShowHomeAdapter.MyHolder>(CategoryDiffUtil) {

    var onLongItemClick: ((CategoryItem) -> Unit)? = null

    class MyHolder(var binding: ItemPopularBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(
            ItemPopularBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        val data = getItem(position)
        Glide.with(holder.itemView)
            .load(data.strCategoryThumb)
            .into(holder.binding.imgPopularItems)

        holder.itemView.setOnClickListener {
            listener.itemCLick(data.strCategory)
        }

        holder.itemView.setOnLongClickListener {
            onLongItemClick?.invoke(data)
            true
        }

    }

    private object CategoryDiffUtil : DiffUtil.ItemCallback<CategoryItem>() {
        override fun areItemsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean {
            return oldItem.idCategory == newItem.idCategory
        }

        override fun areContentsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean {
            return oldItem == newItem
        }

    }

    override fun onViewRecycled(holder: MyHolder) {
        super.onViewRecycled(holder)
        Glide.with(holder.itemView).clear(holder.binding.imgPopularItems)
    }

    interface OnItemCLick {
        fun itemCLick(str: String)
    }

}