package com.devcomentry.noteapp.presentation.model

import com.devcomentry.noteapp.domain.model.MealsByCategory
import com.devcomentry.noteapp.presentation.base.mapper.ItemMapper
import javax.inject.Inject

data class MealByCategoryItem(
    val idMeal: String,
    val strMeal: String,
    val strMealThumb: String
) : ModelItem()

class MealByCategoryItemMapper @Inject constructor() :
    ItemMapper<MealsByCategory, MealByCategoryItem> {
    override fun mapToPresentation(entity: MealsByCategory): MealByCategoryItem {
        return MealByCategoryItem(
            idMeal = entity.idMeal,
            strMealThumb = entity.strMealThumb,
            strMeal = entity.strMeal
        )
    }

    override fun mapToDomain(model: MealByCategoryItem): MealsByCategory {
        TODO("Not yet implemented")
    }

}