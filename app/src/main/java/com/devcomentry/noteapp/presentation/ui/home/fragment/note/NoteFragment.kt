package com.devcomentry.noteapp.presentation.ui.home.fragment.note

import android.content.Intent
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.FragmentNoteBinding
import com.devcomentry.noteapp.presentation.base.fragment.BaseFragment
import com.devcomentry.noteapp.presentation.common.NOTE_ID
import com.devcomentry.noteapp.presentation.ui.add_edit_note.AddNoteActivity
import com.devcomentry.noteapp.presentation.ui.add_edit_note.UpdateNoteActivity
import com.devcomentry.noteapp.presentation.ui.chat_bot.ChatBotActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NoteFragment : BaseFragment<FragmentNoteBinding>(), NoteAdapter.OnItemClick {
    private lateinit var adapter: NoteAdapter
    private val viewModel by viewModels<NoteViewModel>()

    companion object {
        fun newInstance(): NoteFragment {
            return NoteFragment()
        }
    }

    override fun getContentLayout(): Int {
        return R.layout.fragment_note
    }

    override fun initView() {
        viewModel.getAllNotes()
        adapter = NoteAdapter(this)
        binding.apply {
            notesRecyclerView.layoutManager = LinearLayoutManager(requireContext())
            notesRecyclerView.adapter = adapter

            imageAddNotesMain.setOnClickListener {
                startActivity(Intent(requireContext(), AddNoteActivity::class.java))
            }
            tlbNote.txtTitle.text = resources.getString(R.string.strNote)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAllNotes()
    }

    override fun initListener() {
        binding.apply {
            tlbNote.imgChatBot.setOnClickListener {
                val intent = Intent(activity, ChatBotActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun observerLiveData() {
        viewModel.apply {
            getNotes.observe(this@NoteFragment) {
                adapter.submitList(it)
            }
        }
    }

    override fun onNoteClick(noteId: String) {
        val intent = Intent(requireContext(), UpdateNoteActivity::class.java)
        intent.putExtra(NOTE_ID, noteId)
        startActivity(intent)
    }
}