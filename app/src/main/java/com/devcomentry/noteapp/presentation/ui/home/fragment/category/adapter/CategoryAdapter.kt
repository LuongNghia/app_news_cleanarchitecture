package com.devcomentry.noteapp.presentation.ui.home.fragment.category.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.databinding.ItemCategoryBinding
import com.devcomentry.noteapp.presentation.model.CategoryItem

class CategoryAdapter
    (
    private val context: Context,
    private val listener: OnItemCLick
) : ListAdapter<CategoryItem, CategoryAdapter.CategoryViewHolder>(CategoryDiffCallBack) {

    inner class CategoryViewHolder(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: CategoryItem) {
            binding.apply {
                Glide.with(context).load(data.strCategoryThumb).into(imgMealCategory)
                txtCategory.text = data.strCategory
                imgMealCategory.setOnClickListener {
                    listener.itemCLick(data.strCategory)
                }
            }
        }

        fun clear() {
            Glide.with(context).clear(binding.imgMealCategory)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            ItemCategoryBinding.inflate(LayoutInflater.from(context), parent, false)
        )
    }

    override fun onViewRecycled(holder: CategoryViewHolder) {
        super.onViewRecycled(holder)
        holder.clear()
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) =
        holder.bind(getItem(position))


    interface OnItemCLick {
        fun itemCLick(str: String)
    }

    private object CategoryDiffCallBack : DiffUtil.ItemCallback<CategoryItem>() {
        override fun areItemsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean {
            return oldItem.idCategory == newItem.idCategory
        }

        override fun areContentsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean {
            return oldItem.idCategory == newItem.idCategory
        }
    }

}



