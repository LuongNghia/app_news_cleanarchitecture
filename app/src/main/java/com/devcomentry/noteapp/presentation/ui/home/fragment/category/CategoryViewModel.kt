package com.devcomentry.noteapp.presentation.ui.home.fragment.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.devcomentry.noteapp.domain.use_case.server.category.GetCategoryUseCase
import com.devcomentry.noteapp.presentation.base.viewmodel.BaseViewModel
import com.devcomentry.noteapp.presentation.model.CategoryItem
import com.devcomentry.noteapp.presentation.model.CategoryItemMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(
    private val useCase: GetCategoryUseCase,
    private val mapper: CategoryItemMapper
) : BaseViewModel() {

    private var listCategories = MutableLiveData<List<CategoryItem>>()
    val getListCategories = listCategories as LiveData<List<CategoryItem>>
    fun getCategories() {
        parentJob = viewModelScope.launch(handler) {
            val result = useCase.invoke().map {
                mapper.mapToPresentation(it)
            }
            listCategories.value = result
        }
        registerJobFinish()
    }
}