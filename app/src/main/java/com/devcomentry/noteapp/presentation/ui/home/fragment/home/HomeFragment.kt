package com.devcomentry.noteapp.presentation.ui.home.fragment.home

import android.content.Intent
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.FragmentHomeBinding
import com.devcomentry.noteapp.presentation.base.fragment.BaseFragment
import com.devcomentry.noteapp.presentation.common.CATEGORY_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_ID
import com.devcomentry.noteapp.presentation.common.MEAL_NAME
import com.devcomentry.noteapp.presentation.common.MEAL_THUMB
import com.devcomentry.noteapp.presentation.model.CategoryItem
import com.devcomentry.noteapp.presentation.model.MealItem
import com.devcomentry.noteapp.presentation.model.MealsByCategoryItem
import com.devcomentry.noteapp.presentation.ui.category_meal.CategoryMealActivity
import com.devcomentry.noteapp.presentation.ui.chat_bot.ChatBotActivity
import com.devcomentry.noteapp.presentation.ui.detail_meal.DetailMealActivity
import com.devcomentry.noteapp.presentation.ui.home.fragment.home.adapters.CategoriesShowHomeAdapter
import com.devcomentry.noteapp.presentation.ui.home.fragment.home.adapters.FavouritesHomeAdapter
import com.devcomentry.noteapp.presentation.ui.home.fragment.home.adapters.MostPopularAdapter
import com.devcomentry.noteapp.presentation.ui.home.fragment.home.bottomsheet.MealBottomSheetFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(),
    CategoriesShowHomeAdapter.OnItemCLick,
    MostPopularAdapter.OnItemCLick,
    FavouritesHomeAdapter.OnItemCLick {
    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    private val viewModel by viewModels<HomeViewModel>()
    private lateinit var randomMeal: MealItem
    private lateinit var popularAdapter: MostPopularAdapter
    private lateinit var categoriesAdapter: CategoriesShowHomeAdapter
    private lateinit var favouriteAdapter: FavouritesHomeAdapter

    override fun getContentLayout(): Int {
        return R.layout.fragment_home
    }

    override fun initView() {
        binding.txtFavourite.visibility = View.GONE
        binding.recyclerFavourite.visibility = View.GONE
        binding.tlbHome.txtTitle.text = resources.getString(R.string.home)
        prepareCategoriesItemsRecyclerView()
        preparePopularItemsRecyclerView()

        //random meal
        viewModel.getRandomMeal()
        onRandomMealClick()

        //popular items
        viewModel.getPopularItems()
        onPopularItemLongClick()

        //list category
        viewModel.getCategories()

        //favourite meal
        prepareRecyclerViewFavouriteHome()
        viewModel.getAllMealFavourite()
    }

    override fun initListener() {
        binding.apply {
            tlbHome.imgChatBot.setOnClickListener {
                val intent = Intent(activity, ChatBotActivity::class.java)
                startActivity(intent)
            }
        }
    }

    override fun observerLiveData() {
        viewModel.apply {
            randomMealResponse.observe(this@HomeFragment) { meal ->
                meal?.let {
                    Glide.with(this@HomeFragment)
                        .load(it.strMealThumb)
                        .into(binding.imgRandomMeal)

                    randomMeal = it
                }
            }

            mealsByCategoryResponse.observe(this@HomeFragment) { mealList ->
                popularAdapter.submitList(mealList as ArrayList<MealsByCategoryItem>)
            }

            listCategoriesResponse.observe(this@HomeFragment) { categories ->
                categoriesAdapter.submitList(categories as ArrayList<CategoryItem>)
            }

            mealsFavouriteResponse.observe(this@HomeFragment) { meals ->
                if (meals.isNotEmpty()) {
                    binding.apply {
                        txtFavourite.visibility = View.VISIBLE
                        recyclerFavourite.visibility = View.VISIBLE
                    }
                }
                favouriteAdapter.submitList(meals)
            }
        }
    }

    private fun preparePopularItemsRecyclerView() {
        popularAdapter = MostPopularAdapter(this@HomeFragment)
        binding.recyclerViewOver.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            adapter = popularAdapter
        }
    }

    private fun prepareCategoriesItemsRecyclerView() {
        categoriesAdapter = CategoriesShowHomeAdapter(this@HomeFragment)
        binding.recyclerViewCategory.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = categoriesAdapter
        }
    }

    private fun onRandomMealClick() {
        binding.randomMealCard.setOnClickListener {
            val intent = Intent(activity, DetailMealActivity::class.java)
            intent.putExtra(MEAL_ID, randomMeal.idMeal)
            intent.putExtra(MEAL_NAME, randomMeal.strMeal)
            intent.putExtra(MEAL_THUMB, randomMeal.strMealThumb)
            startActivity(intent)
        }
    }

    override fun itemCLick(str: String) {
        val intent = Intent(activity, CategoryMealActivity::class.java)
        intent.putExtra(CATEGORY_NAME, str)
        startActivity(intent)
    }

    override fun itemCLick(strId: String, strName: String, strThumb: String) {
        val intent = Intent(activity, DetailMealActivity::class.java)
        intent.putExtra(MEAL_ID, strId)
        intent.putExtra(MEAL_NAME, strName)
        intent.putExtra(MEAL_THUMB, strThumb)
        startActivity(intent)
    }

    override fun itemFavouriteCLick(strId: String, strName: String, strThumb: String) {
        val intent = Intent(activity, DetailMealActivity::class.java)
        intent.putExtra(MEAL_ID, strId)
        intent.putExtra(MEAL_NAME, strName)
        intent.putExtra(MEAL_THUMB, strThumb)
        startActivity(intent)
    }

    private fun onPopularItemLongClick() {
        popularAdapter.onLongItemClick = { meal ->
            val mealBottomSheetFragment = MealBottomSheetFragment.newInstance(meal.idMeal)
            mealBottomSheetFragment.show(childFragmentManager, "Meal Info")
        }
    }

    private fun prepareRecyclerViewFavouriteHome() {
        favouriteAdapter = FavouritesHomeAdapter(this@HomeFragment)
        binding.recyclerFavourite.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = favouriteAdapter
        }
    }

    override fun onResume() {
        super.onStart()
        viewModel.getAllMealFavourite()
    }

}