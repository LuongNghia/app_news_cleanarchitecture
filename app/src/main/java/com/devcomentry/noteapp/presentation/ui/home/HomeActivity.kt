package com.devcomentry.noteapp.presentation.ui.home

import android.view.MenuItem
import androidx.activity.viewModels
import com.devcomentry.noteapp.R
import com.devcomentry.noteapp.databinding.ActivityHomeBinding
import com.devcomentry.noteapp.presentation.base.activity.BaseActivity
import com.devcomentry.noteapp.presentation.base.fragment.PagerFragmentAdapter
import com.devcomentry.noteapp.presentation.ui.home.fragment.category.CategoriesFragment
import com.devcomentry.noteapp.presentation.ui.home.fragment.favourite.FavouritesFragment
import com.devcomentry.noteapp.presentation.ui.home.fragment.home.HomeFragment
import com.devcomentry.noteapp.presentation.ui.home.fragment.note.NoteFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding>(),
    BottomNavigationView.OnNavigationItemSelectedListener {
    private val homeFragment = HomeFragment.newInstance()
    private val categoriesFragment = CategoriesFragment.newInstance()
    private val searchFragment = FavouritesFragment.newInstance()
    private val noteFragment = NoteFragment.newInstance()
    lateinit var mPagerAdapter: PagerFragmentAdapter
    override fun getContentLayout(): Int {
        return R.layout.activity_home
    }

    override fun initView() {
        binding.apply {

            bottomNav.setOnNavigationItemSelectedListener(this@HomeActivity)
        }
        mPagerAdapter = PagerFragmentAdapter(supportFragmentManager)
        //add all fragment in main
        mPagerAdapter.addFragment(homeFragment)
        mPagerAdapter.addFragment(searchFragment)
        mPagerAdapter.addFragment(categoriesFragment)
        mPagerAdapter.addFragment(noteFragment)
        binding.homeViewPager.adapter = mPagerAdapter
        binding.homeViewPager.offscreenPageLimit = mPagerAdapter.count
    }

    override fun initListener() {
    }

    override fun observerLiveData() {
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                binding.homeViewPager.currentItem = 0
                return true
            }
            R.id.nav_search -> {
                binding.homeViewPager.currentItem = 1
                return true
            }
            R.id.nav_favorite -> {
                binding.homeViewPager.currentItem = 2
                return true
            }
            R.id.nav_note -> {
                binding.homeViewPager.currentItem = 3
                return true
            }
        }
        return false
    }
}