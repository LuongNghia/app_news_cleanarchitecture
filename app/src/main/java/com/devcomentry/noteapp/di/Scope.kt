package com.devcomentry.noteapp.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class JsonPlaceHolderSite(
)

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class MealSite(
)

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ChatSite(
)