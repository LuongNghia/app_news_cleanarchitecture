package com.devcomentry.noteapp.di

import com.devcomentry.noteapp.BuildConfig
import com.devcomentry.noteapp.data.data_source.server.apis.ChatBotAPI
import com.devcomentry.noteapp.data.data_source.server.apis.MealAPI
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    private const val TIME_OUT: Long = 100
    private const val mToken = "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"

    @Provides
    fun provideMealApi(@Named("MealSite") retrofit: Retrofit): MealAPI {
        return retrofit.create(MealAPI::class.java)
    }

    @Provides
    fun provideChatBotApi(@Named("ChatSite") retrofit: Retrofit): ChatBotAPI {
        return retrofit.create(ChatBotAPI::class.java)
    }

    @Provides
    @Singleton
    @Named("ChatSite")
    fun provideChatBotRetrofit(
        moshiConverterFactory: MoshiConverterFactory
    ): Retrofit {
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        httpClient.readTimeout(TIME_OUT, TimeUnit.SECONDS);

        httpClient.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $mToken")
                .build()
            chain.proceed(request)
        }

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging) // <-- this is the important line!
        }
        return Retrofit.Builder().addConverterFactory(moshiConverterFactory)
            .baseUrl(BuildConfig.BASE_URL_CHAT_BOT)
            .client(httpClient.build())
            .build()
    }

    @Provides
    @Singleton
    @Named("MealSite")
    fun provideMealRetrofit(
        okHttpClient: OkHttpClient,
        moshiConverterFactory: MoshiConverterFactory
    ): Retrofit {

        return Retrofit.Builder().addConverterFactory(moshiConverterFactory)
            .baseUrl(BuildConfig.BASE_URL_MEAL)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideOKHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()

        builder.interceptors().add(httpLoggingInterceptor)
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    @Provides
    @Singleton
    fun provideMoshiConverterFactory(): MoshiConverterFactory {
        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        return MoshiConverterFactory.create(moshi)
    }

}