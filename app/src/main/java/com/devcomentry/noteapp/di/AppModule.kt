package com.devcomentry.noteapp.di

import android.content.Context
import androidx.room.Room
import com.devcomentry.noteapp.data.data_source.local.favourite.FavouriteDao
import com.devcomentry.noteapp.data.data_source.local.favourite.FavouriteDatabase
import com.devcomentry.noteapp.data.data_source.local.note.NoteDao
import com.devcomentry.noteapp.data.data_source.local.note.NoteDatabase
import com.devcomentry.noteapp.data.repository.*
import com.devcomentry.noteapp.domain.repository.*
import com.devcomentry.noteapp.domain.repository.local.FavouriteRepository
import com.devcomentry.noteapp.domain.repository.local.NoteRepository
import com.devcomentry.noteapp.domain.repository.server.*
import com.devcomentry.noteapp.domain.use_case.*
import com.devcomentry.noteapp.domain.use_case.local.*
import com.devcomentry.noteapp.domain.use_case.local.note.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): NoteDatabase {
        return Room.databaseBuilder(
            context,
            NoteDatabase::class.java,
            NoteDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    fun provideNoteDao(appDatabase: NoteDatabase): NoteDao {
        return appDatabase.noteDao()
    }

    @Provides
    @Singleton
    fun provideFavouriteDatabase(@ApplicationContext context: Context): FavouriteDatabase {
        return Room.databaseBuilder(
            context,
            FavouriteDatabase::class.java,
            FavouriteDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    fun provideFavouriteDao(appDatabase: FavouriteDatabase): FavouriteDao {
        return appDatabase.favouriteDao()
    }

    @Provides
    @Singleton
    fun provideFavouriteRepository(
        repositoryImpl: FavouriteRepositoryImpl
    ): FavouriteRepository {
        return repositoryImpl
    }

    @Provides
    @Singleton
    fun provideNoteRepository(
        repositoryImpl: NoteRepositoryImpl
    ): NoteRepository {
        return repositoryImpl
    }

    @Provides
    @Singleton
    fun provideMealRepository(repositoryImpl: MealRepositoryImpl): MealRepository {
        return repositoryImpl
    }

    @Provides
    @Singleton
    fun provideMealsByCategoryRepository(repositoryImpl: MealsByCategoryRepositoryImpl): MealsByCategoryRepository {
        return repositoryImpl
    }

    @Provides
    @Singleton
    fun provideCategoryRepository(repositoryImpl: CategoryRepositoryImpl): CategoryRepository {
        return repositoryImpl
    }

    @Provides
    @Singleton
    fun provideChatBotRepository(repositoryImpl: ChatBotRepositoryImpl): ChatBotRepository {
        return repositoryImpl
    }

    @Provides
    @Singleton
    fun provideNoteUseCases(
        repository: NoteRepository
    ): NoteUseCases {
        return NoteUseCases(
            getNotes = GetNotesUseCase(repository),
            deleteNote = DeleteNote(repository),
            addNote = AddNoteUseCase(repository),
            getNote = GetNoteUseCase(repository),
            updateNote = UpdateNote(repository)
        )
    }
}