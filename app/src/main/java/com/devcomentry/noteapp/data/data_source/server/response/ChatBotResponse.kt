package com.devcomentry.noteapp.data.data_source.server.response

import com.squareup.moshi.Json


data class ChatBotResponse(
    val id: String,
    @Json(name = "object")
    val welcome9Object: String,
    val created: Long,
    val model: String,
    val usage: Usage,
    val choices: List<Choice>
)



