package com.devcomentry.noteapp.data.data_source.local.favourite

import androidx.room.*
import com.devcomentry.noteapp.data.model.MealEntity

@Dao
interface FavouriteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMealFavourite(meal: MealEntity)

    @Delete
    suspend fun deleteMealFavourite(meal: MealEntity)

    @Query("select * from mealInformation")
    suspend fun getAllMeals(): List<MealEntity>
}