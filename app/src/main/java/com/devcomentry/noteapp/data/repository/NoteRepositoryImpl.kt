package com.devcomentry.noteapp.data.repository

import com.devcomentry.noteapp.data.base.network.BaseRemoteService
import com.devcomentry.noteapp.data.data_source.local.note.NoteDatabase
import com.devcomentry.noteapp.data.model.NoteEntityMapper
import com.devcomentry.noteapp.domain.model.Note
import com.devcomentry.noteapp.domain.repository.local.NoteRepository
import javax.inject.Inject

class NoteRepositoryImpl @Inject constructor(
    private val noteDao: NoteDatabase,
    private val mapper: NoteEntityMapper,
) : NoteRepository, BaseRemoteService() {

    override suspend fun getNotes(): List<Note> {
        return noteDao.noteDao().getNotes().map { mapper.mapToDomain(it) }
    }

    override suspend fun getNoteById(id: String): Note {
        val notes = noteDao.noteDao().getNoteById(id)
        return mapper.mapToDomain(notes)
    }

    override suspend fun insertNote(note: Note) {
        val mapperNoteEntity = mapper.mapToEntity(note)
        noteDao.noteDao().insertNote(mapperNoteEntity)
    }

    override suspend fun updateNote(note: Note) {
        val mapperNoteEntity = mapper.mapToEntity(note)
        noteDao.noteDao().updateNote(mapperNoteEntity)
    }

    override suspend fun deleteNote(note: Note) {
        val mapperNoteEntity = mapper.mapToEntity(note)
        noteDao.noteDao().deleteNote(mapperNoteEntity)
    }

}