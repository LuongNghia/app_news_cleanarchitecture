package com.devcomentry.noteapp.data.data_source.server.response

import com.devcomentry.noteapp.data.model.CategoryEntity

data class CategoryResponse(
    val categories: List<CategoryEntity>
)