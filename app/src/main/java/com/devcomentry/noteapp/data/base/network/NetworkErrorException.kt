package com.devcomentry.noteapp.data.base.network

public open class NetworkErrorException (val responseMessage: String? = null): Exception() {
}