package com.devcomentry.noteapp.data.model

import com.devcomentry.noteapp.data.base.EntityMapper
import com.devcomentry.noteapp.data.base.ModelEntity
import com.devcomentry.noteapp.domain.model.Message
import javax.inject.Inject

data class MessageEntity(
    val role: String?,
    val content: String?
) : ModelEntity()

class MessageEntityMapper @Inject constructor() : EntityMapper<Message, MessageEntity> {
    override fun mapToDomain(entity: MessageEntity): Message {
        return Message(
            role = entity.role.orEmpty(),
            content = entity.content.orEmpty()
        )
    }

    fun mapToEntity(model: Message): MessageEntity {
        return MessageEntity(
            role = model.role,
            content = model.content
        )
    }
}
