package com.devcomentry.noteapp.data.model

import com.devcomentry.noteapp.data.base.EntityMapper
import com.devcomentry.noteapp.data.base.ModelEntity
import com.devcomentry.noteapp.domain.model.Category
import javax.inject.Inject

data class CategoryEntity(
    val idCategory: String?,
    val strCategory: String?,
    val strCategoryDescription: String?,
    val strCategoryThumb: String?
) : ModelEntity()

class CategoryEntityMapper @Inject constructor() : EntityMapper<Category, CategoryEntity> {
    override fun mapToDomain(entity: CategoryEntity): Category {
        return Category(
            idCategory = entity.idCategory.orEmpty(),
            strCategory = entity.strCategory.orEmpty(),
            strCategoryThumb = entity.strCategoryThumb.orEmpty(),
            strCategoryDescription = entity.strCategoryDescription.orEmpty()
        )
    }

}