package com.devcomentry.noteapp.data.repository

import com.devcomentry.noteapp.data.base.network.BaseRemoteService
import com.devcomentry.noteapp.data.base.network.NetworkResult
import com.devcomentry.noteapp.data.data_source.server.apis.MealAPI
import com.devcomentry.noteapp.data.model.CategoryEntityMapper
import com.devcomentry.noteapp.di.IoDispatcher
import com.devcomentry.noteapp.domain.model.Category
import com.devcomentry.noteapp.domain.repository.server.CategoryRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CategoryRepositoryImpl @Inject constructor(
    private val api: MealAPI,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val mapper: CategoryEntityMapper
) : CategoryRepository, BaseRemoteService() {

    override suspend fun getCategories(): List<Category> = withContext(dispatcher) {
        when (val result = callApi { api.getCategories() }) {
            is NetworkResult.Success -> {
                result.data.categories.map { mapper.mapToDomain(it) }
            }
            is NetworkResult.Error -> {
                throw result.exception
            }
        }
    }

}