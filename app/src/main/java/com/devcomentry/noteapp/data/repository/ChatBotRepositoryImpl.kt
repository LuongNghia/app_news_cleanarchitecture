package com.devcomentry.noteapp.data.repository

import com.devcomentry.noteapp.data.base.network.BaseRemoteService
import com.devcomentry.noteapp.data.base.network.NetworkResult
import com.devcomentry.noteapp.data.data_source.server.apis.ChatBotAPI
import com.devcomentry.noteapp.data.data_source.server.request.ChatBotRequest
import com.devcomentry.noteapp.data.model.MessageEntityMapper
import com.devcomentry.noteapp.di.IoDispatcher
import com.devcomentry.noteapp.domain.model.Message
import com.devcomentry.noteapp.domain.repository.server.ChatBotRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ChatBotRepositoryImpl @Inject constructor(
    private val api: ChatBotAPI,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val mapper: MessageEntityMapper
) : ChatBotRepository, BaseRemoteService() {
    override suspend fun getChat(request: Message): Message = withContext(dispatcher) {
        val messageEntity = mapper.mapToEntity(request)
        val model = "gpt-3.5-turbo"
        when (val result = callApi {
            api.getChats(
                ChatBotRequest(
                    model, listOf(messageEntity)
                )
            )
        }) {
            is NetworkResult.Success -> {
                mapper.mapToDomain(
                    result.data.choices[0].message
                )
            }
            is NetworkResult.Error -> {
                throw result.exception
            }
        }

    }

}