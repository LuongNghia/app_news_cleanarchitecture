package com.devcomentry.noteapp.data.base

import com.devcomentry.noteapp.domain.model.Model

interface EntityMapper<M : Model, ME : ModelEntity> {
    fun mapToDomain(entity: ME): M

    //fun mapToEntity(model: M): ME

}