package com.devcomentry.noteapp.data.data_source.server.response

import com.devcomentry.noteapp.data.model.MealsByCategoryEntity

data class MealsByCategoryResponse (
    val meals: List<MealsByCategoryEntity>)