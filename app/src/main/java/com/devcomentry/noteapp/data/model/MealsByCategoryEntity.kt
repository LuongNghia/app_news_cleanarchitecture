package com.devcomentry.noteapp.data.model

import com.devcomentry.noteapp.data.base.EntityMapper
import com.devcomentry.noteapp.data.base.ModelEntity
import com.devcomentry.noteapp.domain.model.MealsByCategory
import javax.inject.Inject

data class MealsByCategoryEntity(
    val idMeal: String?,
    val strMeal: String?,
    val strMealThumb: String?
) : ModelEntity()

class MealsByCategoryEntityMapper @Inject constructor() :
    EntityMapper<MealsByCategory, MealsByCategoryEntity> {
    override fun mapToDomain(entity: MealsByCategoryEntity): MealsByCategory {
        return MealsByCategory(
            idMeal = entity.idMeal.orEmpty(),
            strMealThumb = entity.strMealThumb.orEmpty(),
            strMeal = entity.strMeal.orEmpty()
        )
    }

}