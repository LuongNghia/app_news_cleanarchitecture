package com.devcomentry.noteapp.data.data_source.server.response

import com.devcomentry.noteapp.data.model.MealEntity

data class MealResponse(val meals: List<MealEntity>)