package com.devcomentry.noteapp.data.repository

import com.devcomentry.noteapp.data.base.network.BaseRemoteService
import com.devcomentry.noteapp.data.data_source.local.favourite.FavouriteDatabase
import com.devcomentry.noteapp.data.model.MealEntityMapper
import com.devcomentry.noteapp.domain.model.Meal
import com.devcomentry.noteapp.domain.repository.local.FavouriteRepository
import javax.inject.Inject

class FavouriteRepositoryImpl @Inject constructor(
    private val favouriteDao: FavouriteDatabase,
    private val mapper: MealEntityMapper,
) : FavouriteRepository, BaseRemoteService() {

    override suspend fun insertMealFavourite(meal: Meal) {
        val mapperEntity = mapper.mapToEntity(meal)
        favouriteDao.favouriteDao().insertMealFavourite(mapperEntity)
    }

    override suspend fun deleteMealFavourite(meal: Meal) {
        val mapperEntity = mapper.mapToEntity(meal)
        favouriteDao.favouriteDao().deleteMealFavourite(mapperEntity)
    }

    override suspend fun getAllMeals(): List<Meal> {
        return favouriteDao.favouriteDao().getAllMeals().map {
            mapper.mapToDomain(it)
        }
    }
}