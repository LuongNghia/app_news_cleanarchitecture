package com.devcomentry.noteapp.data.data_source.local.note

import androidx.room.*
import com.devcomentry.noteapp.data.model.NoteEntity

@Dao
interface NoteDao {
    @Query("select * from noteEntity")
    suspend fun getNotes(): List<NoteEntity>

    @Query("select * from noteEntity where id=:id")
    suspend fun getNoteById(id: String): NoteEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(note: NoteEntity)

    @Update
    suspend fun updateNote(note: NoteEntity)

    @Delete
    suspend fun deleteNote(note: NoteEntity)
}