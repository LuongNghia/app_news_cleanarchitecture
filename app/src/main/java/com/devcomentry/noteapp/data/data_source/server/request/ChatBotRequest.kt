package com.devcomentry.noteapp.data.data_source.server.request

import com.devcomentry.noteapp.data.model.MessageEntity


data class ChatBotRequest(
    val model: String,
    val messages: List<MessageEntity>
)

