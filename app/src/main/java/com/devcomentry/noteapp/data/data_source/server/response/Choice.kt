package com.devcomentry.noteapp.data.data_source.server.response

import com.devcomentry.noteapp.data.model.MessageEntity
import com.squareup.moshi.Json

data class Choice(
    val message: MessageEntity,
    @Json(name = "finish_reason")
    val finishReason: String,
    val index: Long
)