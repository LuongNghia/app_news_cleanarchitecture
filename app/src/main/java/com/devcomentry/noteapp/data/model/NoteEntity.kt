package com.devcomentry.noteapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.devcomentry.noteapp.data.base.EntityMapper
import com.devcomentry.noteapp.data.base.ModelEntity
import com.devcomentry.noteapp.domain.model.Note
import javax.inject.Inject

@Entity
data class NoteEntity(
    @PrimaryKey
    var id: String,
    @ColumnInfo(name = "note_title")
    val noteTitle: String?,
    @ColumnInfo(name = "note_subtitle")
    val noteSubtitle: String?,
    @ColumnInfo(name = "note")
    val note: String?,
    @ColumnInfo(name = "date_time")
    val dateTime: String?,
) : ModelEntity()

class NoteEntityMapper @Inject constructor() : EntityMapper<Note, NoteEntity> {
    override fun mapToDomain(entity: NoteEntity): Note {
        return Note(
            id = entity.id,
            dateTime = entity.dateTime.orEmpty(),
            noteSubtitle = entity.noteSubtitle.orEmpty(),
            note = entity.note.orEmpty(),
            noteTitle = entity.noteTitle.orEmpty()
        )
    }

    fun mapToEntity(model: Note): NoteEntity {
        return NoteEntity(
            id = model.id,
            dateTime = model.dateTime,
            noteSubtitle = model.noteSubtitle,
            note = model.note,
            noteTitle = model.noteTitle
        )
    }

}
