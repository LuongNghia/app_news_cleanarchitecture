package com.devcomentry.noteapp.data.data_source.server.apis

import com.devcomentry.noteapp.data.data_source.server.response.CategoryResponse
import com.devcomentry.noteapp.data.data_source.server.response.MealResponse
import com.devcomentry.noteapp.data.data_source.server.response.MealsByCategoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MealAPI {

    @GET("lookup.php?")
    suspend fun getMealDetails(@Query("i") id: String): Response<MealResponse>

    @GET("random.php")
    suspend fun getRandomMeal(): Response<MealResponse>

    @GET("filter.php?")
    suspend fun getPopularItem(@Query("c") categoryName:String): Response<MealsByCategoryResponse>

    @GET("categories.php")
    suspend fun getCategories(): Response<CategoryResponse>

    @GET("filter.php")
    suspend fun getMealsByCategory(@Query("c") categoryName: String): Response<MealsByCategoryResponse>


}