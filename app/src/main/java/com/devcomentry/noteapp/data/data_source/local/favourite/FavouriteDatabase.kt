package com.devcomentry.noteapp.data.data_source.local.favourite

import androidx.room.Database
import androidx.room.RoomDatabase
import com.devcomentry.noteapp.data.model.MealEntity

@Database(
    entities = [MealEntity::class],
    version = 1
)
abstract class FavouriteDatabase: RoomDatabase() {

    abstract fun favouriteDao(): FavouriteDao

    companion object {
        const val DATABASE_NAME = "favourite_database"
    }
}