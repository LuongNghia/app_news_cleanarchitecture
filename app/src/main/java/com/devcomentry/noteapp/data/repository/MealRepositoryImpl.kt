package com.devcomentry.noteapp.data.repository

import com.devcomentry.noteapp.data.base.network.BaseRemoteService
import com.devcomentry.noteapp.data.base.network.NetworkResult
import com.devcomentry.noteapp.data.data_source.local.note.NoteDatabase
import com.devcomentry.noteapp.data.data_source.server.apis.MealAPI
import com.devcomentry.noteapp.data.model.MealEntityMapper
import com.devcomentry.noteapp.di.IoDispatcher
import com.devcomentry.noteapp.domain.model.Meal
import com.devcomentry.noteapp.domain.repository.server.MealRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MealRepositoryImpl @Inject constructor(
    private val api: MealAPI,
    private val dao: NoteDatabase,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val mapperMeal: MealEntityMapper,
    private val mapper: MealEntityMapper,
) : MealRepository, BaseRemoteService() {
    override suspend fun getDetailMeal(id: String): List<Meal> = withContext(dispatcher) {
        when (val result = callApi { api.getMealDetails(id) }) {
            is NetworkResult.Success -> {
                result.data.meals.map { mapperMeal.mapToDomain(it) }
            }
            is NetworkResult.Error -> {
                throw result.exception
            }
        }
    }

    override suspend fun getRandomMeal(): Meal = withContext(dispatcher) {
        when (val result = callApi { api.getRandomMeal() }) {
            is NetworkResult.Success -> {
                result.data.meals[0].let {
                    mapperMeal.mapToDomain(it)
                }
            }
            is NetworkResult.Error -> {
                throw result.exception
            }
        }
    }
}