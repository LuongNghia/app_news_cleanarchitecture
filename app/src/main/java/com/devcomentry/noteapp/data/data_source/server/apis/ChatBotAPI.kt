package com.devcomentry.noteapp.data.data_source.server.apis

import com.devcomentry.noteapp.data.data_source.server.request.ChatBotRequest
import com.devcomentry.noteapp.data.data_source.server.response.ChatBotResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ChatBotAPI {
    @POST("chat/completions")
    suspend fun getChats(@Body message: ChatBotRequest): Response<ChatBotResponse>
}