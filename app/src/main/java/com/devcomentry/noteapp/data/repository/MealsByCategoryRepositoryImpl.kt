package com.devcomentry.noteapp.data.repository

import com.devcomentry.noteapp.data.base.network.BaseRemoteService
import com.devcomentry.noteapp.data.base.network.NetworkResult
import com.devcomentry.noteapp.data.data_source.server.apis.MealAPI
import com.devcomentry.noteapp.data.model.MealsByCategoryEntityMapper
import com.devcomentry.noteapp.di.IoDispatcher
import com.devcomentry.noteapp.domain.model.MealsByCategory
import com.devcomentry.noteapp.domain.repository.server.MealsByCategoryRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MealsByCategoryRepositoryImpl @Inject constructor(
    private val api: MealAPI,
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val mapperMealsByCategory: MealsByCategoryEntityMapper,
) : MealsByCategoryRepository, BaseRemoteService() {

    override suspend fun getPopularItems(categoryName: String): List<MealsByCategory> =
        withContext(dispatcher) {
            when (val result = callApi { api.getPopularItem(categoryName) }) {
                is NetworkResult.Success -> {
                    result.data.meals.map { mapperMealsByCategory.mapToDomain(it) }
                }
                is NetworkResult.Error -> {
                    throw result.exception
                }
            }
        }

    override suspend fun getMealByCategory(name: String): List<MealsByCategory> =
        withContext(dispatcher) {
            when (val result = callApi { api.getMealsByCategory(name) }) {
                is NetworkResult.Success -> {
                    result.data.meals.map { mapperMealsByCategory.mapToDomain(it) }
                }
                is NetworkResult.Error -> {
                    throw result.exception
                }
            }
        }
}