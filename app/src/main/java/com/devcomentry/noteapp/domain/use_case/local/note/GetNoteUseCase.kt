package com.devcomentry.noteapp.domain.use_case.local.note

import com.devcomentry.noteapp.domain.model.Note
import com.devcomentry.noteapp.domain.repository.local.NoteRepository
import javax.inject.Inject

class GetNoteUseCase @Inject constructor(private val noteRepository: NoteRepository) {

    suspend operator fun invoke(id: String): Note? {
        return noteRepository.getNoteById(id)
    }
}