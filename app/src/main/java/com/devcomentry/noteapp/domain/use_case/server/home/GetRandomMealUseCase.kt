package com.devcomentry.noteapp.domain.use_case.server.home

import com.devcomentry.noteapp.domain.model.Meal
import com.devcomentry.noteapp.domain.repository.server.MealRepository
import javax.inject.Inject

class GetRandomMealUseCase @Inject constructor(
    private val repository: MealRepository
) {
    suspend operator fun invoke(): Meal {
        return repository.getRandomMeal()
    }
}