package com.devcomentry.noteapp.domain.repository.local

import com.devcomentry.noteapp.domain.model.Note


interface NoteRepository {
    suspend fun getNotes(): List<Note>

    suspend fun getNoteById(id: String): Note?

    suspend fun insertNote(note: Note)

    suspend fun updateNote(note: Note)

    suspend fun deleteNote(note: Note)

}