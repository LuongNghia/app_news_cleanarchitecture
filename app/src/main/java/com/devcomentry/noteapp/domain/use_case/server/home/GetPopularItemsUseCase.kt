package com.devcomentry.noteapp.domain.use_case.server.home

import com.devcomentry.noteapp.domain.model.MealsByCategory
import com.devcomentry.noteapp.domain.repository.server.MealsByCategoryRepository
import javax.inject.Inject

class GetPopularItemsUseCase @Inject constructor(
    private val repositoryMealsByCategory: MealsByCategoryRepository
) {
    suspend operator fun invoke(categoryName: String): List<MealsByCategory> {
        return repositoryMealsByCategory.getPopularItems(categoryName)
    }
}