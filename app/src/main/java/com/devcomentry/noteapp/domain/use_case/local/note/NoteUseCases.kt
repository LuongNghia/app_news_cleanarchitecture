package com.devcomentry.noteapp.domain.use_case.local.note

data class NoteUseCases(
    val getNotes: GetNotesUseCase,
    val deleteNote: DeleteNote,
    val addNote: AddNoteUseCase,
    val updateNote: UpdateNote,
    val getNote: GetNoteUseCase
)