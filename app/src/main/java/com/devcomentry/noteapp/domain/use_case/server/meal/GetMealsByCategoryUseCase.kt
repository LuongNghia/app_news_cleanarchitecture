package com.devcomentry.noteapp.domain.use_case.server.meal

import com.devcomentry.noteapp.domain.model.MealsByCategory
import com.devcomentry.noteapp.domain.repository.server.MealsByCategoryRepository
import javax.inject.Inject

class GetMealsByCategoryUseCase @Inject constructor(
    private val repository: MealsByCategoryRepository
) {
    suspend operator fun invoke(name: String): List<MealsByCategory> {
        return repository.getMealByCategory(name)
    }
}