package com.devcomentry.noteapp.domain.model

data class Message(
    val role: String,
    val content: String
) : Model()
