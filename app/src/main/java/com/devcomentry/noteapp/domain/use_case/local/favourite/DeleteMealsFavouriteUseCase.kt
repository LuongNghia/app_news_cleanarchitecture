package com.devcomentry.noteapp.domain.use_case.local.favourite

import com.devcomentry.noteapp.domain.model.Meal
import com.devcomentry.noteapp.domain.repository.local.FavouriteRepository
import javax.inject.Inject

class DeleteMealsFavouriteUseCase @Inject constructor(
    private val favouriteRepository: FavouriteRepository
) {

    suspend operator fun invoke(meal: Meal) {
        favouriteRepository.deleteMealFavourite(meal)
    }
}