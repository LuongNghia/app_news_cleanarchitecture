package com.devcomentry.noteapp.domain.repository.local

import com.devcomentry.noteapp.domain.model.Meal

interface FavouriteRepository {

    suspend fun insertMealFavourite(meal: Meal)

    suspend fun deleteMealFavourite(meal: Meal)

    suspend fun getAllMeals(): List<Meal>
}