package com.devcomentry.noteapp.domain.repository.server

import com.devcomentry.noteapp.domain.model.Message

interface ChatBotRepository {
    suspend fun getChat(request: Message): Message
}