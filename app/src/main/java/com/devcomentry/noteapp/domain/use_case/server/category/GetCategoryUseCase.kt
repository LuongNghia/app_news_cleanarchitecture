package com.devcomentry.noteapp.domain.use_case.server.category

import com.devcomentry.noteapp.domain.model.Category
import com.devcomentry.noteapp.domain.repository.server.CategoryRepository
import javax.inject.Inject

class GetCategoryUseCase @Inject constructor(
    private val repository: CategoryRepository
) {
    suspend operator fun invoke(): List<Category> {
        return repository.getCategories()
    }
}