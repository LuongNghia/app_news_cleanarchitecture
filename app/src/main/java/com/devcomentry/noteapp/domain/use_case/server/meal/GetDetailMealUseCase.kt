package com.devcomentry.noteapp.domain.use_case.server.meal

import com.devcomentry.noteapp.domain.model.Meal
import com.devcomentry.noteapp.domain.repository.server.MealRepository
import javax.inject.Inject

class GetDetailMealUseCase @Inject constructor(
    private val repository: MealRepository
) {
    suspend operator fun invoke(id: String): List<Meal> {
        return repository.getDetailMeal(id)
    }
}