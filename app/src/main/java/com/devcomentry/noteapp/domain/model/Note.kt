package com.devcomentry.noteapp.domain.model

data class Note(
    var id: String,
    val noteTitle: String,
    val noteSubtitle: String,
    val note: String,
    val dateTime: String,
) : Model()