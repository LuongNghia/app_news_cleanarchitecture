package com.devcomentry.noteapp.domain.repository.server

import com.devcomentry.noteapp.domain.model.Category

interface CategoryRepository {
    suspend fun getCategories(): List<Category>
}