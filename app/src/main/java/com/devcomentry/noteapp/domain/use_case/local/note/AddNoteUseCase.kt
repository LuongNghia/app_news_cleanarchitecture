package com.devcomentry.noteapp.domain.use_case.local.note

import com.devcomentry.noteapp.domain.model.Note
import com.devcomentry.noteapp.domain.repository.local.NoteRepository
import javax.inject.Inject

class AddNoteUseCase @Inject constructor(
    private val noteRepository: NoteRepository
) {

    suspend operator fun invoke(note: Note) {
        noteRepository.insertNote(note)
    }
}