package com.devcomentry.noteapp.domain.repository.server

import com.devcomentry.noteapp.domain.model.MealsByCategory

interface MealsByCategoryRepository {

    suspend fun getPopularItems(categoryName: String): List<MealsByCategory>

    suspend fun getMealByCategory(name: String): List<MealsByCategory>

}