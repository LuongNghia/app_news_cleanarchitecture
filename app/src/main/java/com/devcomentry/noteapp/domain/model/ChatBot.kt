package com.devcomentry.noteapp.domain.model

data class ChatBot(
    val model: String,
    val messages: List<Message>
)