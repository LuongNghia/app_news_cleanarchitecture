package com.devcomentry.noteapp.domain.use_case.server.chat_bot

import com.devcomentry.noteapp.domain.model.Message
import com.devcomentry.noteapp.domain.repository.server.ChatBotRepository
import javax.inject.Inject

class SendChatUseCase @Inject constructor(
    private val repository: ChatBotRepository
) {
    suspend operator fun invoke(message: Message): Message {
        return repository.getChat(message)
    }
}