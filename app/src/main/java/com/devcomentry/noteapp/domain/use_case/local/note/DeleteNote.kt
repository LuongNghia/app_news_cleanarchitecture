package com.devcomentry.noteapp.domain.use_case.local.note

import com.devcomentry.noteapp.domain.model.Note
import javax.inject.Inject
import com.devcomentry.noteapp.domain.repository.local.NoteRepository

class DeleteNote @Inject constructor(
    private val noteRepository: NoteRepository
) {
    suspend operator fun invoke(note: Note) {
        noteRepository.deleteNote(note)
    }
}