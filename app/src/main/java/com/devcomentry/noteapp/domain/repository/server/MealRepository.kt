package com.devcomentry.noteapp.domain.repository.server

import com.devcomentry.noteapp.domain.model.Meal

interface MealRepository {

    suspend fun getDetailMeal(id: String): List<Meal>

    suspend fun getRandomMeal(): Meal
}