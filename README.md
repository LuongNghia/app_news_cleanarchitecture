# App_Cooking_CleanArchitecture

+ Phân chia công việc:
+ UI:
- Nghĩa: Home, Note(show note, add note), Favourite, ChatBot
- Tuân: Category, Note(edit note), detail meal , meal by category

+ Logic(Call API and data local):
- Nghĩa: API randomMeal, category home,random mealByCategory, save favourite(save, delete,  undo) (data local)
- Tuân: API categories, mealByCategory, Detail meal ,save Note(edit, add, delete, show) (data local)
- Tuân and Nghĩa: Chat bot 
